# ENTREGA CONVOCATORIA ENERO
Carlos Peinado Martin c.peinadom.2023@alumnos.urjc.es
https://youtu.be/d7cmerMXwy4 
## Requisitos mínimos
generar_patron_aleatorio
generar_patron_cuadros
generar_patron_rectangulos
analizar_brillos
ordenar_por_brillo
rotar_textura
reorganizar_por_componente
transformar_a_grises
## Requisitos opcionales
generar_degradado
ajustar_contraste
comprimir_colores
generar_patron_custom