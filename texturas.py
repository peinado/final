from pyqtgraph.util.cprint import color

from pixels import pinta_lienzo, espera, cargar_lienzo_desde_imagen

import random


def generar_patron_aleatorio(ancho, alto):
    """
    Genera un lienzo con colores aleatorios.

    Args:
        ancho (int): Número de píxeles de ancho.
        alto (int): Número de píxeles de alto.

    Returns:
        dict: Lienzo representado como un diccionario.
    """
    if isinstance(ancho, int) and isinstance(alto, int):
        patron = {"width": ancho,
                  "height": alto,
                  "pixels": []}
        for y in range(alto):
            linea = []
            for x in range(ancho):
                pixel = [random.randint(0, 255), random.randint(0, 255),
                         random.randint(0, 255)]
                linea.append(pixel)
            patron["pixels"].append(linea)
        return patron
    else:
        raise TypeError("Alto o ancho incorrectos, han de ser numeros enteros")


def generar_patron_cuadros(ancho, alto, color1, color2):
    """
    Genera un lienzo con un patrón de cuadros.

    Args:
        ancho (int): Número de píxeles de ancho.
        alto (int): Número de píxeles de alto.
        color1 (tuple): Color de los cuadros claros en formato RGB.
        color2 (tuple): Color de los cuadros oscuros en formato RGB.

    Returns:
        dict: Lienzo representado como un diccionario.
    """
    if isinstance(ancho, int) and isinstance(alto, int):
        if isinstance(color1, tuple) and isinstance(color2, tuple):
            pixels = []
            for x in range(ancho):
                linea = []
                for y in range(alto):
                    if (x + y) % 2 == 0: #par
                        linea.append(color1)
                    else: #impar
                        linea.append(color2)
                pixels.append(linea)
            dict = {
                "height": alto,
                "width": ancho,
                "pixels": pixels
            }
            return dict
        else:
            raise TypeError("Los colores han de ser listas")
    else:
        raise TypeError("Alto o ancho incorrectos, han de ser numeros enteros")


def generar_patron_rectangulos(ancho, alto, rectangulos):
    """
    Genera un lienzo con un patrón de rectángulos.

    Args:
        ancho (int): Número de píxeles de ancho del lienzo.
        alto (int): Número de píxeles de alto del lienzo.
        rectangulos (list): Lista de rectángulos a dibujar.
        Cada rectángulo es un diccionario con:
            - 'x': Coordenada X de la esquina superior izquierda.
            - 'y': Coordenada Y de la esquina superior izquierda.
            - 'ancho': Ancho del rectángulo.
            - 'alto': Alto del rectángulo.
            - 'color': Color del rectángulo en formato RGB.
            - 'relleno': Booleano, si es True, el rectángulo estará relleno.

    Returns:
        dict: Lienzo representado como un diccionario
         con los rectángulos dibujados.
    """
    if isinstance(ancho, int) and isinstance(alto, int):
        if isinstance(rectangulos, list):
            pixels = []
            for x in range(ancho):
                linea = []
                for y in range(alto):
                    linea.append((255, 255, 255))
                pixels.append(linea)
            for rectangulo in rectangulos:
                x_rec = rectangulo["x"]
                y_rec = rectangulo["y"]
                ancho_rec = rectangulo["ancho"]
                alto_rec = rectangulo["alto"]
                color_rec = rectangulo["color"]
                relleno = rectangulo["relleno"]
                for y in range(y_rec, y_rec + alto_rec):
                    for x in range(x_rec, x_rec + ancho_rec):
                        if relleno:
                            pixels[y][x] = color_rec
                        else: #Solo borde
                            if (y == y_rec
                                    or y == y_rec + alto_rec - 1
                                    or x == x_rec
                                    or x == x_rec + ancho_rec - 1):
                                pixels[y][x] = color_rec
            patron = {
                "height": alto,
                "width": ancho,
                "pixels": pixels
            }
            return patron
        else:
            raise TypeError("Rectangulos a de ser una lista")
    else:
        raise TypeError("Alto o ancho incorrectos, han de ser numeros enteros")


def calcula_brillo(pixel):
    return (pixel[0] + pixel[1] + pixel[2]) // 3


def analizar_brillos(lienzo):
    """
    Analiza los brillos de los píxeles del lienzo.

    Args:
        lienzo (dict): Lienzo representado como un diccionario.

    Returns:
        dict: Diccionario con claves 'brillos', 'promedio', 'max', 'min'.
    """
    if isinstance(lienzo, dict):
        brillos = []
        max_brillo = 0
        min_brillo = 255
        for y in range(lienzo["height"]):
            for x in range(lienzo["width"]):
                brillo = calcula_brillo(lienzo["pixels"][y][x])
                brillos.append(brillo)
                if brillo > max_brillo:
                    max_brillo = brillo
                if brillo < min_brillo:
                    min_brillo = brillo
        promedio = sum(brillos) / len(brillos)
        analisis = {
            "brillos": brillos,
            "promedio": promedio,
            "max": max_brillo,
            "min": min_brillo
        }
        return analisis
    else:
        raise TypeError("Lienzo ha de ser un diccionario")


def ordenar_por_brillo(lienzo, orden="ascendente"):
    """
    Ordena los píxeles del lienzo por brillo usando el algoritmo de selección.

    Args:
        lienzo (dict): Lienzo representado como un diccionario.
        orden (str): 'ascendente' o 'descendente'.

    Returns:
        dict: Nuevo lienzo con los píxeles ordenados por brillo.
    """
    if isinstance(lienzo, dict):
        if orden == "ascendente" or orden == "descendente":
            pixels_plano = []
            for fila in lienzo["pixels"]:
                for pixel in fila:
                    pixels_plano.append(pixel) #pixeles en una lista plana
            for i in range(len(pixels_plano)):
                indice = i
                for j in range(i + 1, len(pixels_plano)):
                    if orden == "ascendente" and (calcula_brillo(pixels_plano[j]) < calcula_brillo(pixels_plano[indice])):
                        indice = j
                    elif orden == "descendente" and (calcula_brillo(pixels_plano[j]) > calcula_brillo(pixels_plano[indice])):
                        indice = j
                if indice != i: #si fueran iguales no funcionaria
                    pivote = pixels_plano[indice]
                    pixels_plano[indice] = pixels_plano[i]
                    pixels_plano[i] = pivote
    # regresamos los pixeles a una lista de listas, para poder usar coordenadas x e y
            contador = 0
            ordenado = []
            for y in range(lienzo["height"]):
                linea = []
                for x in range(lienzo["width"]):
                    linea.append(pixels_plano[contador])
                    contador += 1
                ordenado.append(linea)
            lienzo_new = {
                "alto": lienzo["height"],
                "ancho": lienzo["width"],
                "pixels": ordenado
            }
            return lienzo_new
        else:
            raise ValueError("Orden tiene que ser descendente o ascendente")
    else:
        raise TypeError("Lienzo ha de ser un diccionario")


def rotar_textura(lienzo, angulo):
    """
    Rota el lienzo en el ángulo especificado.

    Args:
        lienzo (dict): Lienzo representado como un diccionario.
        angulo (int): Ángulo de rotación (90, 180, 270).

    Returns:
        dict: Lienzo rotado.
    """
    if isinstance(lienzo, dict):
        if angulo == 90:
            pixels_new = []
            for x in range(lienzo["width"]):
                linea = []
                for y in range(lienzo["height"] - 1, - 1, - 1):
                    linea.append(lienzo["pixels"][y][x])
                pixels_new.append(linea)
            lienzo_new = {
                "alto": lienzo["height"],
                "ancho": lienzo["width"],
                "pixels": pixels_new
            }
            return lienzo_new
        elif angulo == 180:
            pixels_new = []
            for y in range(lienzo["height"] - 1, - 1, - 1):
                linea = []
                for x in range(lienzo["width"] - 1, - 1, - 1):
                    linea.append(lienzo["pixels"][y][x])
                pixels_new.append(linea)
            lienzo_new = {
                "alto": lienzo["height"],
                "ancho": lienzo["width"],
                "pixels": pixels_new
            }
            return lienzo_new
        elif angulo == 270:
            pixels_new = []
            for x in range(lienzo["width"] - 1, - 1, - 1):
                linea = []
                for y in range(lienzo["height"]):
                    linea.append(lienzo["pixels"][y][x])
                pixels_new.append(linea)
            lienzo_new = {
                "alto": lienzo["height"],
                "ancho": lienzo["width"],
                "pixels": pixels_new
            }
            return lienzo_new
        else:
            raise ValueError("Grado ha de ser 90, 180 o 270")
    else:
        raise TypeError("Lienzo ha de ser un diccionario")


def reorganizar_por_componente(lienzo, componente):
    """
    Reorganiza el lienzo dividiéndolo en cuadrantes según
    una componente (R, G o B).

    Args:
        lienzo (dict): Lienzo representado como un diccionario.
        componente (str): Componente del color ('r', 'g' o 'b').

    Returns:
        dict: Lienzo reorganizado por la componente seleccionada.
    """
    if isinstance(lienzo, dict):
        if componente == 'r' or componente == 'g' or componente == 'b':
            indice_comp = 0
            if componente == 'r':
                indice_comp = 0
            elif componente == 'g':
                indice_comp = 1
            elif componente == 'b':
                indice_comp = 2
            cuad1 = []
            cuad2 = []
            cuad3 = []
            cuad4 = []
            for fila in lienzo["pixels"]:
                for pixel in fila:
                    if pixel[indice_comp] <= 63:
                        cuad1.append(pixel)
                    elif 63 < pixel[indice_comp] <= 127:
                        cuad2.append(pixel)
                    elif 127 < pixel[indice_comp] <= 191:
                        cuad3.append(pixel)
                    elif 191 < pixel[indice_comp]:
                        cuad4.append(pixel)
            pixel_organizado = cuad1 + cuad2 + cuad3 + cuad4
            pixels_new = []
            contador = 0
            for y in range(lienzo["height"]):
                linea = []
                for x in range(lienzo["width"]):
                    linea.append(pixel_organizado[contador])
                    contador += 1
                pixels_new.append(linea)

            lienzo_new = {
                "height": lienzo["height"],
                "width": lienzo["width"],
                "pixels": pixels_new
            }
            return lienzo_new
        else:
            raise ValueError("Componente ha de ser r, g o b")
    else:
        raise TypeError("Lienzo ha de ser un diccionario")


def transformar_a_grises(lienzo):
    """
    Convierte el lienzo a escala de grises.

    Args:
        lienzo (dict): Lienzo representado como un diccionario.

    Returns:
        dict: Lienzo transformado a escala de grises.
    """
    if isinstance(lienzo, dict):
        pixels_new = []
        for fila in lienzo["pixels"]:
            linea_new = []
            for pixel in fila:
                pomedio = sum(pixel) // 3
                gris = (pomedio, pomedio, pomedio)
                linea_new.append(gris)
            pixels_new.append(linea_new)

        lienzo_new = {
            "height": lienzo["height"],
            "width": lienzo["width"],
            "pixels": pixels_new
        }
        return lienzo_new
    else:
        raise TypeError("Lienzo ha de ser un diccionario")


########## PARTE OPCIONAL ##########


def generar_degradado(ancho, alto, color_inicio, color_fin):
    """
    Genera un lienzo con un degradado entre dos colores.

    Args:
        ancho (int): Ancho del lienzo.
        alto (int): Alto del lienzo.
        color_inicio (tuple): Color inicial en formato RGB.
        color_fin (tuple): Color final en formato RGB.

    Returns:
        dict: Lienzo con el degradado aplicado.
    """
    lienzo = {"width": ancho,
              "height": alto,
              "pixels": []}
    for y in range(alto):
        linea = []
        for x in range(ancho):
            r = int(color_inicio[0] + (color_fin[0] -
                    color_inicio[0]) * x / (ancho - 1))
            g = int(color_inicio[1] + (color_fin[1] -
                    color_inicio[1]) * x / (ancho - 1))
            b = int(color_inicio[2] + (color_fin[2] -
                    color_inicio[2]) * x / (ancho - 1))
            linea.append((r, g, b))
        lienzo["pixels"].append(linea)
    return lienzo


def ajustar_contraste(lienzo, factor):
    """
    Ajusta el contraste del lienzo.

    Args:
        lienzo (dict): Lienzo representado como un diccionario.
        factor (float): Factor de ajuste de contraste (>1 aumenta, <1 reduce).

    Returns:
        dict: Lienzo con el contraste ajustado.
    """
    referencia = 128
    pixels_new = []
    for fila in lienzo["pixels"]:
        linea_new = []
        for pixel in fila:
            r, g, b = pixel
            r_new = int(((r - referencia) * factor) + referencia)
            g_new = int(((g - referencia) * factor) + referencia)
            b_new = int(((b - referencia) * factor) + referencia)
            if 0 > r_new:
                r_new = 0
            elif r_new > 255:
                r_new = 255
            if 0 > g_new:
                g_new = 0
            elif g_new > 255:
                g_new = 255
            if 0 > b_new:
                b_new = 0
            elif b_new > 255:
                b_new = 255
            linea_new.append((r_new, g_new, b_new))
        pixels_new.append(linea_new)
    lienzo_new = {
        "height": lienzo["height"],
        "width": lienzo["width"],
        "pixels": pixels_new
    }
    return lienzo_new


def comprimir_colores(lienzo, niveles):
    """
    Reduce la cantidad de colores del lienzo agrupándolos en niveles definidos.

    Args:
        lienzo (dict): Lienzo representado como un diccionario.
        niveles (int): Número de niveles de color.

    Returns:
        dict: Lienzo con los colores comprimidos.
    """
    intervalo = 256 // niveles
    pixels_new = []
    for fila in lienzo["pixels"]:
        linea_new = []
        for pixel in fila:
            r = pixel[0] // intervalo * intervalo
            g = pixel[1] // intervalo * intervalo
            b = pixel[2] // intervalo * intervalo
            linea_new.append((r, g, b))
        pixels_new.append(linea_new)

    lienzo_new = {
        "height": lienzo["height"],
        "width": lienzo["width"],
        "pixels": pixels_new
    }
    return lienzo_new


def generar_patron_custom(ancho, alto, configuracion, color_fondo):
    """
    Genera un lienzo según una configuración específica de
    posiciones y colores.

    Args:
        ancho (int): Ancho del lienzo.
        alto (int): Alto del lienzo.
        configuracion (dict): Diccionario con coordenadas (x, y) y colores RGB.
        color_fondo (tuple): Color de fondo.

    Returns:
        dict: Lienzo generado.
    """
    pixels_new = []
    for y in range(alto):
        linea_new = []
        for x in range(ancho):
            if (x, y) in configuracion:
                linea_new.append(configuracion[(x, y)])
            else:
                linea_new.append(color_fondo)
        pixels_new.append(linea_new)

    lienzo_new = {
        "height": alto,
        "width": ancho,
        "pixels": pixels_new
    }
    return lienzo_new


def main():
    lienzo_aleatorio = generar_patron_aleatorio(20, 20)
    pinta_lienzo(lienzo_aleatorio)
    espera()


if __name__ == "__main__":
    main()
